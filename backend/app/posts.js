const express = require('express');
const multer = require('multer');
const router = express.Router();
const path = require('path');
const nanoid = require('nanoid');
const Post = require('../models/Post');
const User = require('../models/User');
const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {
    router.get('/', (req, res) => {
        Post.find().populate('user')
            .then(result => res.send(result.sort((a, b) => {
                return new Date(a.datetime) - new Date(b.datetime);
            }).reverse()))
            .catch(() => res.sendStatus(500));
    });

    router.post('/', upload.single('image'), async (req, res) => {
        const token = req.get('Token');
        const post = req.body;

        if (!token) {
            return res.status(401).send({error: 'Token is not found'});
        }

        const user = await User.findOne({token: token});

        if (!user) {
            return res.status(401).send({error: 'User is not authorized'});
        }

        post.user = user._id;

        if (req.file) {
            post.image = req.file.filename;
        } else if (!post.description && !post.image) {
            return res.status(401).send({error: 'Description field or image field should not be empty!'});
        } else {
            post.image = 'message.jpg';
        }

        const p = new Post(post);

        await p.save();

        return res.send(p);

    });

    router.get('/:id', (req, res) => {
        Post.find({_id: req.params.id}).populate('user')
            .then(result => res.send(result[0]))
            .catch(error => res.status(400).send(error));
    });

    return router;
};

module.exports = createRouter;