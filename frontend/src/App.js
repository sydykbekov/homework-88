import React, {Component} from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import Posts from "./containers/Posts/Posts";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Layout from "./containers/Layout/Layout";
import NewPost from "./containers/NewPost/NewPost";
import PostInfo from "./containers/PostInfo/PostInfo";

class App extends Component {
    render() {
        return (
            <Layout>
                <Switch>
                    <Route path="/" exact component={Posts}/>
                    <Route path="/register" exact component={Register}/>
                    <Route path="/login" exact component={Login}/>
                    <Route path="/create" exact component={NewPost}/>
                    <Route path="/post/:id" exact component={PostInfo}/>
                </Switch>
            </Layout>
        );
    }
}

export default App;
