import React from 'react';
import {Col, ControlLabel, FormControl, FormGroup, HelpBlock} from "react-bootstrap";
import PropTypes from 'prop-types';

const FormElement = props => (
    <FormGroup controlId={props.propertyName} validationState={props.error && 'error'}>
        <Col componentClass={ControlLabel} sm={2}>
            {props.title}
        </Col>
        <Col sm={10}>
            <FormControl
                type={props.type}
                placeholder={props.placeholder}
                name={props.propertyName}
                value={props.value}
                onChange={props.changeHandler}
                required={props.required}
                autoComplete={props.autoComplete}
            />
            {props.error && <HelpBlock>{props.error}</HelpBlock>}
        </Col>
    </FormGroup>
);

FormElement.propTypes = {
    propertyName: PropTypes.string.isRequired,
    error: PropTypes.string,
    title: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    changeHandler: PropTypes.func.isRequired,
    required: PropTypes.bool,
    autoComplete: PropTypes.string
};

export default FormElement;