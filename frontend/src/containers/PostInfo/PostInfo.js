import React, {Component, Fragment} from 'react';
import {getComments, getInfo, sendComment} from "../../store/actions/posts";
import {connect} from "react-redux";
import {Button, Col, Form, FormGroup, Image, Panel} from "react-bootstrap";
import FormElement from "../../components/UI/Form/FormElement";

class PostInfo extends Component {
    state = {
        description: ''
    };
    inputChangeHandler = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };
    submitFormHandler = event => {
        event.preventDefault();

        const formData = {...this.state, postId: this.props.match.params.id};

        this.props.onSubmit(formData, this.props.user.token);
    };

    componentDidMount() {
        this.props.getInfo(this.props.match.params.id);
        this.props.getComments(this.props.match.params.id);
    }
    render() {
        let content;
        if (this.props.post) {
            content = (
                <Panel>
                    <Panel.Body>
                        <h3>{this.props.post.title}</h3>
                        <Image
                            style={{width: '300px', marginRight: '10px'}}
                            src={'http://localhost:8000/uploads/' + this.props.post.image}
                            thumbnail
                        />
                        <p>{this.props.post.description}</p>
                        <h4>{this.props.post.datetime} by {this.props.post.user.username}</h4>
                    </Panel.Body>
                    {this.props.comments.map(comment => <Panel bsStyle="info" style={{width: '90%', margin: '10px auto'}} key={comment._id}>
                        <Panel.Heading>
                            <h4>{comment.user.username}:</h4>
                            {comment.description}
                        </Panel.Heading>
                    </Panel>)}
                </Panel>
            )
        } else {
            content = <div>Loading ...</div>
        }

        return (
            <Fragment>
                {content}
                {this.props.user && <Panel>
                    <Panel.Body>
                        <Form horizontal onSubmit={this.submitFormHandler}>
                            <FormElement
                                propertyName="description"
                                title="Description"
                                placeholder="description"
                                autoComplete="new-text"
                                type="text"
                                value={this.state.description}
                                changeHandler={this.inputChangeHandler}
                                required
                            />
                            <FormGroup>
                                <Col smOffset={2} sm={10}>
                                    <Button
                                        bsStyle="primary"
                                        type="submit"
                                    >Create comment</Button>
                                </Col>
                            </FormGroup>
                        </Form>
                    </Panel.Body>
                </Panel>}
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    post: state.posts.postInfo,
    user: state.users.user,
    comments: state.posts.comments
});

const mapDispatchToProps = dispatch => ({
    getInfo: id => dispatch(getInfo(id)),
    onSubmit: (formData, token) => dispatch(sendComment(formData, token)),
    getComments: id => dispatch(getComments(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(PostInfo);