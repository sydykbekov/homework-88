import axios from 'axios';
import {push} from "react-router-redux";
export const GET_POSTS_SUCCESS = 'GET_POSTS_SUCCESS';
export const SUCCESS_GET_POST_INFO = 'SUCCESS_GET_POST_INFO';
export const SUCCESS_GET_COMMENTS = 'SUCCESS_GET_COMMENTS';
export const CREATE_POST_ERROR = 'CREATE_POST_ERROR';
export const CREATE_POST_SUCCESS = 'CREATE_POST_SUCCESS';

const getPostsSuccess = posts => {
    return {type: GET_POSTS_SUCCESS, posts};
};

const successGetInfo = postInfo => {
    return {type: SUCCESS_GET_POST_INFO, postInfo};
};

const successGetComments = comments => {
    return {type: SUCCESS_GET_COMMENTS, comments};
};

export const createPostError = error => {
    return {type: CREATE_POST_ERROR, error};
};

export const createPostSuccess = error => {
    return {type: CREATE_POST_SUCCESS, error};
};

export const getPosts = () => {
    return dispatch => {
        axios.get('posts').then(response => {
            dispatch(getPostsSuccess(response.data));
        })
    }
};

export const createPost = (formData, token) => {
    return dispatch => {
        const headers = {"Token": token};
        axios.post('posts', formData, {headers}).then(() => {
            dispatch(createPostSuccess());
            dispatch(push('/'));
        }, error => {
            dispatch(createPostError(error.response.data.error));
        })
    }
};

export const getInfo = id => {
    return dispatch => {
        axios.get(`posts/${id}`).then(response => {
            dispatch(successGetInfo(response.data));
        })
    }
};

export const getComments = id => {
    return dispatch => {
        axios.get(`comments/${id}`).then(response => {
            dispatch(successGetComments(response.data))
        })
    }
};

export const sendComment = (formData, token) => {
    return dispatch => {
        const headers = {"Token": token};
        axios.post('comments', formData, {headers}).then(() => {
            dispatch(getComments(formData.postId));
        })
    }
};
