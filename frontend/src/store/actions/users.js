import axios from 'axios';
import {push} from 'react-router-redux';

export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';

export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';
export const LOGOUT_USER = 'LOGOUT_USER';

const registerUserSuccess = () => {
    return {type: REGISTER_USER_SUCCESS}
};

const registerUserFailure = (error) => {
    return {type: REGISTER_USER_FAILURE, error}
};

export const registerUser = userData => {
    return dispatch => {
        return axios.post('users', userData).then(
            response => {
                dispatch(registerUserSuccess());
                dispatch(push('/'));
            },
            error => {
                dispatch(registerUserFailure(error.response.data));
            }
        );
    };
};

const loginUserSuccess = (user) => {
    return {type: LOGIN_USER_SUCCESS, user};
};

const loginUserFailure = error => {
    return {type: LOGIN_USER_FAILURE, error};
};

export const loginUser = userData => {
    return dispatch => {
        return axios.post('users/sessions', userData).then(response => {
            dispatch(loginUserSuccess(response.data.user));
            dispatch(push('/'));
        }, error => {
            const err = error.response ? error.response.data : {error: 'No internet connection'};
            dispatch(loginUserFailure(err));
        });
    };
};

export const logoutUser = () => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {'Token': token};
        axios.delete('/users/sessions', {headers}).then(
            response => {
                dispatch({type: LOGOUT_USER});
                dispatch(push('/'));
            },
            error => {
                alert('Could not logout');
            }
        );
    }
};
