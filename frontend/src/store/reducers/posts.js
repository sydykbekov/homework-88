import {
    CREATE_POST_ERROR, CREATE_POST_SUCCESS, GET_POSTS_SUCCESS, SUCCESS_GET_COMMENTS,
    SUCCESS_GET_POST_INFO
} from "../actions/posts";

const initialState = {
    posts: [],
    postInfo: null,
    comments: [],
    error: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_POSTS_SUCCESS:
            return {...state, posts: action.posts};
        case SUCCESS_GET_POST_INFO:
            return {...state, postInfo: action.postInfo};
        case SUCCESS_GET_COMMENTS:
            return {...state, comments: action.comments};
        case CREATE_POST_ERROR:
            return {...state, error: action.error};
        case CREATE_POST_SUCCESS:
            return {...state, error: null};
        default:
            return state;
    }
};

export default reducer;